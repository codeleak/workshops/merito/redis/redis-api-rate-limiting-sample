package pl.codeleak.samples.redis;

import java.time.Duration;

public interface RateLimiter {
    /**
     * Checks if the user is allowed to perform an action.
     *
     * @param userId      user identifier
     * @param maxRequests maximum number of requests allowed in the given time window
     * @param timeWindow  time window
     * @return true if the user is allowed to perform an action, false otherwise
     */
    boolean isAllowed(String userId, int maxRequests, Duration timeWindow);
}
