package pl.codeleak.samples.redis;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;

@RestController
public class TestController {

    @Autowired
    private RateLimiter rateLimiter;

    @Autowired
    private HttpServletRequest request;

    @GetMapping("/test")
    public String testEndpoint() {
        var userId = request.getRemoteAddr(); // Use IP address as user identifier
        var maxRequests = 5; // Max requests allowed
        var timeWindow = Duration.ofSeconds(30); // Time window

        if (rateLimiter.isAllowed(userId, maxRequests, timeWindow)) {
            return "Request successful for user: " + userId + " at " + System.currentTimeMillis() + "ms";
        } else {
            return "Too many requests for user: " + userId + " at " + System.currentTimeMillis() + "ms";
        }
    }
}
