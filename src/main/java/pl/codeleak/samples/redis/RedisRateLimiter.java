package pl.codeleak.samples.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
public class RedisRateLimiter implements RateLimiter {

    private static final String RATE_LIMITER_PREFIX = "rate_limiter:";

    @Autowired
    private RedisTemplate<String, Integer> redisTemplate;

    public boolean isAllowed(String userId, int maxRequests, Duration timeWindow) {
        // TODO Implement rate limiting logic using Redis
        //  Use userId as a key to store the number of requests
        //  Use maxRequests to set the maximum number of requests allowed in the given time window
        //  Use timeWindow to set the expiration time for the key
        //  Return true if the user is allowed to perform an action, false otherwise
        //  Hint: You can use command like INCR to increment the value of a key since it stored as an integer

        return false;
    }
}
